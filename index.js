export default {
	async checkSessionState () {
		//	use async-await or promises
		
		
		if (appsmith.mode != "EDIT"){  
			// if the app is in edit mode, navigate to page "content"
			// for any other mode, check if the user is authorized to see the content
			if (!appsmith.store.accessToken ) {
				navigateTo("login")
			} else if (appsmith.store.accessToken === "") {
				navigateTo("login")
			}
			
		}
	}
}